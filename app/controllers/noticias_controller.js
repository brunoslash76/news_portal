//                       ####  noticias PLURAL  ####
module.exports.noticias = function (app, req, res) {

    var connection = app.config.db_connection();
    var noticiasDAO = new app.app.models.NoticiasDAO(connection);

    noticiasDAO.getNoticias(function (erro, result) {

        if(!result) {
            res.render("noticias/noticias", { noticias: result });
        }

        res.render("noticias/noticias", { noticias: result });

    });// fim do noticias_model....
}// fim da func

// DELETAR NOTICIAS
module.exports.deletar = function (app, req, res, id_noticia) {
    var connection = app.config.db_connection();
    var noticiasDAO = new app.app.models.NoticiasDAO(connection);

    var id_noticia = req.query;

    noticiasDAO.deletarNoticia(id_noticia, function (erro, result) {
        if(erro) throw erro;
    });

    noticiasDAO.getNoticias(function (err, result) {
        res.render('home', {noticias: result});
    });
}


//                        ####  noticia SINGULAR  ####
module.exports.noticia = function (app, req, res) {

    var connection = app.config.db_connection();
    var noticiasDAO = new app.app.models.NoticiasDAO(connection);

    var id_noticia = req.query;

    noticiasDAO.getNoticia(id_noticia, function (erro, result) {
        res.render("noticias/noticia", { noticia: result });
    });

}// fim da finc