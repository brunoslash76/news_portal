var express = require('express');
var consign = require('consign');
var bodyParser = require('body-parser');
var express_validator = require('express-validator');

var app = express();
app.set('view engine', 'ejs');
app.set('views', './app/views');

app.use(express.static('./app/public'));
app.use(bodyParser.urlencoded({extended : true}));
app.use(express_validator());

consign()
    .include('app/routes')
    .then('config/db_connection.js')// é necessário identificar o tipo de arquivo que será carregado
    .then('app/models')
    .then('app/controllers')
    .into(app);
// funcao consign para incluir todos os caminhos da app
// funcao include seleciona a pasta de rotas
// funcao into, seleciona o objeto a ser apontado para rodar

module.exports = app;