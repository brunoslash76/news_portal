module.exports = function ( app ) {
    // NOTICIAS PLURAL
    app.get('/noticias', function (req, res) {
        app.app.controllers.noticias_controller.noticias(app, req, res);
    });// fim do app get

    // NOTICIA SINGULAR
    app.get('/noticia', function (req, res) {
        app.app.controllers.noticias_controller.noticia(app, req, res);
    });// fim do app get

    app.get('/deletar', function (req, res) {
        app.app.controllers.noticias_controller.deletar(app, req, res);
    });
}// fim da func
