create database portal_noticias;
use portal_noticias;

create table noticias (
	id_noticia bigint primary key unique not null auto_increment,
    titulo varchar(50) not null,
    resumo varchar(200) not null,
    autor varchar(70) not null, 
    data_criacao timestamp not null,
    noticia blob not null
)  Engine innoDb;