module.exports.formulario_inclusao_noticia = function (app, req, res) {
    res.render("admin/form_add_noticia", { validacao: {}, noticia: {}, msg: '' });
}// fim da func

module.exports.noticias_salvar = function (app, req, res) {
    var noticia = req.body;

    req.assert('titulo', 'Título é obrigatório').notEmpty();
    req.assert('resumo', 'Resumo é obrigatório').notEmpty();
    req.assert('resumo', 'Resumo deve conter entre 10 e 100 caracteres').len(5, 100);
    req.assert('autor', 'Autor é obrigatório').notEmpty();
    req.assert('data_criacao', 'Data é obrigatório').notEmpty().isDate({ format: 'YYYY-MM-DD' });
    req.assert('noticia', 'Notícia é obrigatório').notEmpty();
    // manter o parâmetro "noticias" para noticia no sigular
    // banco de dados com o valor no sigular



    var errors = req.validationErrors();
    if (errors) {
        res.render("admin/form_add_noticia", { validacao: errors, noticias: noticia });
        return;
    }
    // daqui para baixo acontece conforme o desvio de fluxo acima.
    var connection = app.config.db_connection();
    var noticiasDAO = new app.app.models.NoticiasDAO(connection);

    noticiasDAO.salvarNoticia(noticia, function (erro, result) {
        if (erro) console.error(erro);
    });// fim do noticias_model....

    noticiasDAO.getNoticias(function (err, result) {
        if(err) throw err;
        res.render('admin/form_add_noticia', {msg: 'Mensagem salva', validacao: {}})
    });
}// fim da func



