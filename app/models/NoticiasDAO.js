function NoticiasDAO(connection) {
    this._connection = connection;
}
/**
 * GET NOTICIAS [ PLURAL ]
 * Método que retorna todas as noticias
 */
NoticiasDAO.prototype.getNoticias = function (callback) {
    // this._connection.query('select * from noticias order by data_noticia desc ', callback);
    this._connection.query('select * from noticias', callback);
}

NoticiasDAO.prototype.deletarNoticia = function (id_noticia, callback) {
    this._connection.query('delete from noticias where id_noticia = ' + id_noticia.id_noticia, callback);
}

/**
 * GET NOTICIA [ SINGULAR ]
 * Método que retorna conteúdo de uma noticia somente
 * 
 * @param callbal
 */
NoticiasDAO.prototype.getNoticia = function (id_noticia, callback) {
    var id = id_noticia.id_noticia;
    this._connection.query('select * from noticias where id_noticia = ' + id, callback);
}

/**
 * SALVAR NOTICIA
 * Método para salvar dados no BD
 * 
 * @param noticia traz um JSON (Array de infos assim por dizer)
 * @param callback 
 */
NoticiasDAO.prototype.salvarNoticia = function (noticia, callback) {
    let sql = `insert into noticias (titulo, resumo, autor, data_criacao, noticia) values ('${noticia.titulo}',
                    '${noticia.resumo}', '${noticia.autor}', '${noticia.data_criacao}', '${noticia.noticia}');`;

    this._connection.query(sql, noticia, callback);
}

/**
 * GET 5 ULTIMAS NOTICIAS
 * Função que retorna as 5 ultimas noticias adicionadas no BD
 * 
 * @param callback
 */
NoticiasDAO.prototype.get5UltimasNoticias = function (callback) {
    var res = this._connection.query('select * from noticias order by data_criacao desc limit 5', callback);
    if (!res) {
        console.log('vazio');
        res = 1;
        return res;
    }
    return res;
}


// EXPORT DA CLASSE
module.exports = function (app) {
    return NoticiasDAO;
}// fim da function