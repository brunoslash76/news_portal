// Criando uma conecção com o banco de dados
var mysql = require('mysql');

var connMysql = function() {

    console.log('### Conexão com o banco de dados foi estabelecida');

    return mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '12345678',
        database: 'portal_noticias',
        insecureAuth: true,
    });

    
};

module.exports = function () {
    console.log('O auto load carregou o módulo de conexão com o BD');
    return connMysql;
};// fim da func
// Para não criar conexões com banco de dados, vamos embrulhar a função em uma variavel.