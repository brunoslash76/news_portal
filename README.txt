# Portal de Notícias 

This project is part of a trainning course which introduces simple concepts of NodeJS and MySQL. 
In this project I could achieve the creation of a server, using Express and other tools, creating views using EJS, and connecting to MySQL, it's a simple CRUD application.

# How to install 

Clone the project on you machine, open a terminal inside the project folder and run `npm i`, this will install all the dependencies.
Install MySQL and execute the commands in the file `noticias.sql`. 
Edit file `db_connections.js` and set the properties `user` and `password` following the config you setup for your database.
Run the command `node app.js` and access [localhost:3000](http://localhost:3000)
